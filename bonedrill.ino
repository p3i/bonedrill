// Import hardware libraries
#include <Adafruit_MLX90614.h>
#include <HX711.h>
#include <L298N.h>

// Stages of the algorithm
enum class Stage {
  BEFORE_DRILLING,
  FIRST_CORTICAL,
  MAIN_FIRST_CORTICAL,
  TRABECULAR,
  SECOND_CORTICAL,
  AFTER_DRILLING
};

enum class Scenario {
  STOP_TRABECULAR, // Stop before drilling trabecular layer
  FULL // Drill through full bone
};

Stage stage = Stage::BEFORE_DRILLING; // Default to before drilling
Scenario scenario;

// Pin constants - load cell and linear actuator pins on the Arduino
namespace Pin {
  using p = const uint8_t; // 8-bit unsigned integer
  p LOADCELL_DOUT_PIN = 3;
  p LOADCELL_SCK_PIN = 2;
  p LINEAR_ACTUATOR_EN = 6;
  p LINEAR_ACTUATOR_IN1 = 7;
  p LINEAR_ACTUATOR_IN2 = 8;
}

// Threshold to enter/exit the bone
const float VOLTAGE_THRESHOLD = 1500000.0f; 
// Offset for calibration
const float ZERO_OFFSET = 380000.0f;
// Threshold between two measurements
const float DELTA_THRESHOLD = 69000.0f; 

const unsigned short ACTUATOR_SPEED = 102; 
// 102 = 4mm/s. Full speed is 255 (10mm/s).

// Objects / Variables
float last = ZERO_OFFSET; // Last measurement for change in measurements

Adafruit_MLX90614 mlx = Adafruit_MLX90614(); // Infrared thermometer
HX711 loadcell; // Load cell (Force sensor)
L298N linear_actuator(Pin::LINEAR_ACTUATOR_EN, Pin::LINEAR_ACTUATOR_IN1, Pin::LINEAR_ACTUATOR_IN2); // Linear actuator

// Function to choose scenario - single cortical layer / full bone
void choose_scenario() {
  Serial.println("Choose a scenario:");
  Serial.println("Single cortical: '1' or 's'");
  Serial.println("Whole bone: '2' or 'f'");
  Serial.println("No movement: Anything else");
  while (Serial.available() == 0) {}
  char c = Serial.read();
  Serial.print("Input received: ");
  Serial.println(c);
  switch (c) {
    case '1':
    case 's':
      scenario = Scenario::STOP_TRABECULAR;
      break;
    case '2':
    case 'f':
      scenario = Scenario::FULL;
      break;
    default:
      Serial.println("Disallowed scenario.");
      scenario = Scenario::FULL;
      stage = Stage::AFTER_DRILLING;
      break;
  }
}

// Initial setup
void setup() {
  Serial.begin(9600); // Standard baud rate

  choose_scenario(); // Call function to choose scenario

  mlx.begin(); // Begin temperature sensor


  // Set pin modes to output/input
  pinMode(Pin::LINEAR_ACTUATOR_EN, OUTPUT);
  pinMode(Pin::LINEAR_ACTUATOR_IN1, OUTPUT);
  pinMode(Pin::LINEAR_ACTUATOR_IN2, OUTPUT);
  pinMode(Pin::LOADCELL_DOUT_PIN, INPUT);
  pinMode(Pin::LOADCELL_SCK_PIN, INPUT);

  // Begin load cell
  loadcell.begin(Pin::LOADCELL_DOUT_PIN, Pin::LOADCELL_SCK_PIN);
  loadcell.set_scale();
  loadcell.set_gain(128);

  // Sets stage to AFTER_DRILLING if choose_scenario input is invalid.
  if (stage == Stage::AFTER_DRILLING) return; 
  
  // Set speed of linear actuator
  setSignedSpeed(ACTUATOR_SPEED);
}

// Helper function to set speed of linear actuator
void setSignedSpeed(int speed) {
  linear_actuator.setSpeed(abs(speed)); // need to set speed and then run
  linear_actuator.run(speed == 0 ? L298N::STOP : (speed > 0 ? L298N::FORWARD : L298N::BACKWARD)); // positive = forwards, negative = backwards
}


// Stops (retracts) the drill
void stopDrill() {
  setSignedSpeed(-255); // Max speed backwards
  stage = Stage::AFTER_DRILLING; // Sets stage to after drilling
  Serial.print("\tStage: AFTER_DRILLING"); 
}

// Main loop function
void loop() {
  // Get load cell readings
  float force_voltage = loadcell.get_units(1); // modify
  // library for HX711 uses float

  // Get temperature readings
  double tempC = mlx.readObjectTempC();

  // Format console output
  Serial.print(millis());
  Serial.print('\t');
  Serial.print(tempC);
  Serial.print('\t');
  Serial.print((force_voltage - ZERO_OFFSET)/68171.4f);
  Serial.print('\t');
  Serial.print(force_voltage);

  // Stop drill if temperature greater than 47⁰C
  if (tempC > 47) {
    stopDrill();
  }
  
  if (force_voltage >= 0) { // remove fluctuation/error values
    switch (stage) {
      case Stage::BEFORE_DRILLING:
        // Force voltage above threshold 
        if (force_voltage >= VOLTAGE_THRESHOLD) {
          stage = Stage::FIRST_CORTICAL; // Enter first cortical
          Serial.print("\tStage: FIRST_CORTICAL");
        }
        break;
      case Stage::FIRST_CORTICAL:
        // If stopping before trabecular layer
        if (scenario == Scenario::STOP_TRABECULAR) { 
          // Force voltage below threshold 
          // OR change in voltage above threshold
          if (force_voltage < VOLTAGE_THRESHOLD || last - force_voltage > DELTA_THRESHOLD) {
            stopDrill();
            break;
          }
        } else {
          // Force voltage below threshold 
          if (force_voltage < VOLTAGE_THRESHOLD) {
            stage = Stage::TRABECULAR;
            Serial.print("\tStage: TRABECULAR");
            last = ZERO_OFFSET;
            break;
          }
        }
        last = force_voltage;
        break;
      case Stage::TRABECULAR:
        // Force voltage above threshold 
        if (force_voltage >= VOLTAGE_THRESHOLD) {
          stage = Stage::SECOND_CORTICAL;
          Serial.print("\tStage: SECOND_CORTICAL");
        }
        break;
      case Stage::SECOND_CORTICAL:
        // Force voltage below threshold 
        // OR change in voltage above threshold
        if (force_voltage < VOLTAGE_THRESHOLD || last - force_voltage > DELTA_THRESHOLD) {
          stopDrill();
        }
        last = force_voltage;
        break;
      case Stage::AFTER_DRILLING:
        break;
    }
  }
  // Check for user control input, manually controlling drill
  if (Serial.available() > 0) {
    char input = Serial.read();
    Serial.print("\tInput received: ");
    Serial.print(input);
    switch (input) {
      case '0': // Stop drilling and pause linear actuator
        stage = Stage::AFTER_DRILLING;
        setSignedSpeed(0);
        break;
      case 'q': // quit
      case 'r': // reverse
        stopDrill();
        break;
      case 'f': // forward
        setSignedSpeed(255);
        break;
      case '4': // 4 mm/s
        setSignedSpeed(102);
        break;
      default:
        Serial.print(" - Unknown input.");
        break;
    }
  }
  Serial.println();
}